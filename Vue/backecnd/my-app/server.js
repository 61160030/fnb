const express = require('express')
const app = express()

app.get('/', (req, res) => {
  res.json({ message: 'Hi Slendey' })
})

app.listen(9000, () => {
  console.log('running 9000')
})
