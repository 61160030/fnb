import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Form from '../views/Form.vue'
import ShowView from '../views/ShowView.vue'
import Table from '../views/Table.vue'
import Counter from '../views/Counter.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/form',
    name: 'form',
    component: Form
  },
  {
    path: '/showview',
    name: 'showview',
    component: ShowView
  },
  {
    path: '/table',
    name: 'table',
    component: Table
  },
  {
    path: '/counter',
    name: 'counter',
    component: Counter
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue')
  },
  {
    path: '/users',
    name: 'users',
    component: () => import('../views/Users2')
  }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
