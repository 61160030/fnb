const express = require('express')
const router = express.Router()

router.get('/:message', (req, res, next) => {
  const { params } = req
  res.json({
    message: 'Hi I am slendy',
    params
  })
})

module.exports = router
